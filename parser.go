package main

import (
	"bufio"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"path/filepath"
	"os"
	"strconv"
	"strings"
	"os/exec"
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
)

type Findx struct {
	Offset    int64
	Timestamp float64
	PacketLen int
}

type Report struct {
	Filenames []string `xml:"configuration>fileobject>filename"`
}

func InvokeTcpFlow(pcapPath string, outDir string) {
	version, err := exec.Command("/usr/bin/tcpflow", "--version").CombinedOutput()
	if err != nil{
		vs := strings.Fields(string(version))
		log.Println("tcp-flow version:", vs[1])
	}
	result, err := exec.Command("/usr/bin/tcpflow", "-I", "-r", pcapPath, "-o", outDir).CombinedOutput()
	/**cmd := exec.Command("/home/freddy/Downloads/tcpflow/usr/bin/tcpflow", "-I", "-r", pcapPath, "-o", outDir)
	cmd.Env = append(os.Environ(),
		"LD_LIBRARY_PATH=/home/freddy/Downloads/tcpflow/usr/lib/",
	)
	result, err := cmd.CombinedOutput()
	**/
	if err != nil {
		log.Println("tcp-flow exited with non-zero code, but could be ok, don't worry bro.")
	}
	log.Printf(string(result))
}

func getReport(confFile string) Report {
	xmlFile, err := os.Open(confFile)
	// if we os.Open returns an error then handle it
	if err != nil {
		log.Fatal(err)
	}

	fmt.Println("Successfully Opened report.xml")
	// defer the closing of our xmlFile so that we can parse it later on
	defer xmlFile.Close()

	// read our opened xmlFile as a byte array.
	byteValue, _ := ioutil.ReadAll(xmlFile)

	// we initialize our Users array
	var report Report
	// we unmarshal our byteArray which contains our
	// xmlFiles content into 'users' which we defined above
	xml.Unmarshal(byteValue, &report)
	return report

}

func parseFindx(filename string) []Findx {
	indexFile := fmt.Sprintf("%s.findx", filename)
	file, err := os.Open(indexFile)

	if err != nil {
		log.Fatalf("%s index file not present!", indexFile)
	}
	scanner := bufio.NewScanner(file)
	var indexes []Findx

	for scanner.Scan() {
		red := scanner.Text()
		indexData := strings.Split(red, "|")
		offset, _ := strconv.ParseInt(indexData[0],10, 64)
		timestamp, _ := strconv.ParseFloat(indexData[1], 64)
		packetLen, _ := strconv.Atoi(indexData[2])
		indexes = append(indexes, Findx{offset, timestamp, packetLen})
	}

	return indexes

}

func ParseAndPopulate(config Config) {
	report := getReport(filepath.Join(config.TempPath,"report.xml"))
	log.Println("Creating database...")
	database, _ := sql.Open("sqlite3", config.DBPath)
	defer database.Close()
	tx, _ := database.Begin()

	statement, _ := tx.Prepare("CREATE TABLE IF NOT EXISTS traffic (id INTEGER PRIMARY KEY, ip_src TEXT, p_src INT, ip_dest TEXT, p_dest INT, timestamp REAL, content BLOB)")
	statement.Exec()

	log.Println("Loading the packets...")


	for _, filename := range report.Filenames {
		//fmt.Println(filename)
		sp := strings.Split(filename, "/")
		name := sp[len(sp)-1]
		ips := strings.Split(name, "-")
		ports1 := strings.Split(ips[0], ".")
		port1 := ports1[len(ports1)-1]
		ip1 := strings.Replace(ips[0],fmt.Sprintf(".%s", port1), "", -1)
		ports2 := strings.Split(ips[1], ".")
		port2 := ports2[len(ports2)-1]
		ip2 := strings.Replace(ips[1],fmt.Sprintf(".%s", port2), "", -1)
		file,_ := os.Open(filename)
		for _, index := range parseFindx(filename) {
			//Seek
			file.Seek(index.Offset, 0)
			packet := make([]byte, index.PacketLen)
			_, err:= file.Read(packet)
			if err != nil {
				log.Fatal(err)
			}

			tx.Exec("INSERT INTO traffic (ip_src, p_src, ip_dest, p_dest, timestamp, content) VALUES (?, ?, ?, ?, ?, ?)",ip1, port1, ip2, port2, index.Timestamp, packet)
		}
		file.Close()
	}
	tx.Commit()

	log.Println("Packets loaded!")
}