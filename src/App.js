import React from 'react';
import {
    Navbar,
    Nav, Button
} from 'react-bootstrap';
import './App.css';
import {NavigationPanel} from "./components/app/NavigationPanel";
import NavItem from "react-bootstrap/NavItem";
import {LinkContainer} from 'react-router-bootstrap'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link, Redirect
} from "react-router-dom";
import SearchPanel from "./components/app/SearchPanel";

export class App extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            currentFilter: null,
        };

        this.onFilterSelect = this.onFilterSelect.bind(this);
    }

    onFilterSelect(filter) {
        this.setState({
            currentFilter: filter,
        });
    }

    render() {
        return (
            <div>
                <Router>
                    <div>
                        <Navbar variant="dark">
                            <Navbar.Brand href="/">tarik</Navbar.Brand>
                            <Navbar.Collapse id="basic-navbar-nav">
                                <Nav className="mr-auto">
                                    <LinkContainer to="/">
                                        <NavItem>Dashboard</NavItem>
                                    </LinkContainer>
                                    <LinkContainer to="/search">
                                        <NavItem>Search</NavItem>
                                    </LinkContainer>
                                    <LinkContainer to="/navigate">
                                        <NavItem>Navigate</NavItem>
                                    </LinkContainer>
                                </Nav>
                            </Navbar.Collapse>
                        </Navbar>
                    </div>
                    <div className="container-fluid mt-3">
                        <Switch>
                            <Route exact path="/">
                                Ciao
                            </Route>
                            <Route exact path="/search">
                                <SearchPanel
                                    onFilterSelect={this.onFilterSelect}
                                />
                            </Route>
                            <Route exact path="/navigate">
                                <NavigationPanel
                                    currentFilter={this.state.currentFilter}
                                />
                            </Route>
                        </Switch>
                    </div>
                </Router>
            </div>
        )
    }
}

export default App;
