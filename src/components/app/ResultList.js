import React from 'react';
import {ResultItem} from "./ResultItem";

export class ResultList extends React.Component {
    render() {
        return (
            <div className="list-group list-group-hover">
                {this.props.connections.map(conn => {
                    return <ResultItem
                        key={conn.id}
                        connection={conn}
                        onItemSelect={this.props.onItemSelect}
                        isSelected={conn.id === this.props.selectedConnection.id}

                    />
                })}
            </div>
        );
    }
}