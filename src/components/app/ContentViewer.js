import React from 'react';
import {ContentItem} from "./ContentItem";

export class ContentViewer extends React.Component {
    render() {
        return (
            <div>
                <h2 className="mb-3">Connection #{this.props.current.id}</h2>
                {this.props.current.packets.map(packet => {
                    let isRisky = false;
                    if (this.props.filterPackets[this.props.current.id] !== undefined) {
                        if (this.props.filterPackets[this.props.current.id].includes(packet.id)) {
                            isRisky = true;
                        }
                    }

                    return <ContentItem
                        key={packet.id}
                        packet={packet}
                        source={this.props.current.ip_src}
                        hexView={this.props.hexView}
                        syntaxView={this.props.syntaxView}
                        isRisky={isRisky}
                    />
                })}
            </div>
        );
    }
}