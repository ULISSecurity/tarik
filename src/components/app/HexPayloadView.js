import React from 'react';

function toHexString(byteArray) {
    return Array.from(byteArray, function(byte) {
        return ('0' + (byte & 0xFF).toString(16)).slice(-2);
    }).join('')
}

export class HexPayloadView extends React.Component {
    render() {
        let uint8Array = this.props.contentArray;

        let lines = [];

        for (let i = 0; i<uint8Array.length; i+= 16) {
            let hexLine = ("0000000" + i.toString(16)).substr(-8);
            let currentLineTokens = [hexLine];
            for (let k = 0; k<16 && (i+k) < uint8Array.length; k++) {
                let hexChar = ('0' + (uint8Array[i+k] & 0xFF).toString(16)).slice(-2).toUpperCase();
                currentLineTokens.push(hexChar);
            }

            // Generate the printable string
            let chars = []
            for (let k = 0; k<16 && (i+k) < uint8Array.length; k++) {
                if (uint8Array[i+k] >=32 && uint8Array[i+k] < 126) {
                    let c = String.fromCharCode(uint8Array[i+k]);
                    chars.push(c);
                }else{
                    chars.push(".");
                }
            }

            currentLineTokens.push(chars.join(""));

            let currentLine = currentLineTokens.join(" ");
            lines.push(currentLine);
        }
        let content = lines.map(line => <p>{line}</p>);

        return (
            <div className="monospaced">
                {content}
            </div>
        );
    }
}