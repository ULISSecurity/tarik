import React from 'react';
import SyntaxHighlighter from 'react-syntax-highlighter';
import { xt256 } from 'react-syntax-highlighter/dist/esm/styles/hljs';

export class SyntaxPayloadView extends React.Component {
    render() {
        return (
            <SyntaxHighlighter language={this.props.language} style={xt256}>
                {atob(this.props.content)}
            </SyntaxHighlighter>
        );
    }
}