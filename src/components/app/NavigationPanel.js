import React from 'react';
import {ResultList} from "./ResultList";
import {ContentViewer} from "./ContentViewer";
import {ResultItem} from "./ResultItem";

const limit = 100;
const languages = [null, "http", "html"];

export class NavigationPanel extends React.Component {
    offset = 0;
    currentService = null;

    constructor(props) {
        super(props)

        this.state = {
            connections: [],
            currentIndex: 0,
            current: null,
            connectionCount: 0,
            syntaxView: "http",
            hexView: false,
            filterPackets: {},
            services: [],
        };

        this.onItemSelect = this.onItemSelect.bind(this);
        this.onToggleHex = this.onToggleHex.bind(this);
        this.onToggleSyntax = this.onToggleSyntax.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.onNextPage = this.onNextPage.bind(this);
        this.onPreviousPage = this.onPreviousPage.bind(this);
        this.onPreviousConnection = this.onPreviousConnection.bind(this);
        this.onNextConnection = this.onNextConnection.bind(this);
        this.onServiceSelected = this.onServiceSelected.bind(this);
    }

    componentDidMount() {
        this.loadConnections();

        document.addEventListener("keydown", this.handleKeyDown);
    }

    componentWillUnmount() {
        document.removeEventListener("keydown", this.handleKeyDown);
    }

    handleKeyDown(e) {
        if (e.keyCode === 37) {  // Left arrow
            this.onPreviousConnection();
        }else if (e.keyCode === 39) {  // Right arrow
            this.onNextConnection();
        }else if (e.keyCode === 88) {  // X key
            this.onToggleHex();
        }else if (e.keyCode === 90) {  // Z key
            this.onToggleSyntax();
        }else if (e.keyCode === 89) {  // Y key
            this.onPreviousPage();
        }else if (e.keyCode === 85) {  // U key
            this.onNextPage();
        }else if (e.keyCode === 72) {  // H key
            this.onPreviousConnection();
        }else if (e.keyCode === 74) {  // J key
            this.scrollDownConnection();
        }else if (e.keyCode === 75) {  // K key
            this.scrollUpConnection();
        }else if (e.keyCode === 76) {  // L Key
            this.onNextConnection();
        }
    }

    loadConnections() {
        let filter = -1;
        if (this.props.currentFilter !== null) {
            filter = this.props.currentFilter.id;
        }

        let service = -1;
        if (this.currentService !== null) {
            service = this.currentService.port;
        }

        fetch(`/navigate?limit=${limit}&offset=${this.offset}&filter=${filter}&service=${service}`)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        connections: result.connections,
                        current: result.connections[0],
                        currentIndex: 0,
                        offset: result.start,
                    });
                },
                (error) => {
                    alert("Connection error!");
                }
            );

        fetch(`/statistics?filter=${filter}`)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        connectionCount: result.connection_number
                    });
                },
                (error) => {
                    alert("Connection error!");
                }
            );

        fetch(`/services`)
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        services: result,
                    });
                },
                (error) => {
                    alert("Connection error!");
                }
            );

        if (this.props.currentFilter !== null) {
            fetch(`/filter_packets?filter=${filter}`)
                .then(res => res.json())
                .then(
                    (result) => {
                        this.setState({
                            filterPackets: result
                        });
                    },
                    (error) => {
                        alert("Connection error!");
                    }
                );
        }
    }

    onItemSelect(item) {
        this.setState({
            current: item,
        });

        this.scrollToTopConnection();
    }

    onToggleHex() {
        this.setState({
            hexView: !this.state.hexView
        })
    }

    onToggleSyntax() {
        let currentIndex = languages.findIndex((i) => i === this.state.syntaxView);
        let language = languages[currentIndex+1];
        if (currentIndex >= (languages.length-1)) {
            language = languages[0];
        }
        this.setState({
            syntaxView: language
        })
    }

    onNextPage() {
        this.offset = this.offset + limit;
        this.loadConnections();
    }

    onPreviousPage() {
        if (this.state.offset > 0) {
            this.offset = this.offset - limit;
        }
    }

    onNextConnection() {
        if (this.state.currentIndex < (this.state.connections.length-1)) {
            this.setState({
                current: this.state.connections[this.state.currentIndex+1],
                currentIndex: this.state.currentIndex +1,
            })
        }

        this.scrollToTopConnection();
    }

    onPreviousConnection() {
        if (this.state.currentIndex > 0) {
            this.setState({
                current: this.state.connections[this.state.currentIndex-1],
                currentIndex: this.state.currentIndex -1,
            })
        }

        this.scrollToTopConnection();
    }

    scrollDownConnection() {
        document.getElementById("content-box").scrollBy(0, 100);
    }
    scrollUpConnection() {
        document.getElementById("content-box").scrollBy(0, -100);
    }

    scrollToTopConnection() {
        document.getElementById("content-box").scrollTo(0,0);
    }

    onServiceSelected(service) {
        this.currentService = service;
        this.loadConnections();
    }

    render() {
        let hexText = (this.state.hexView) ? "[X] Hex ON" : "[X] Hex OFF";
        let syntaxText = "[Z] Syntax OFF";
        if (this.state.syntaxView !== null) {
            syntaxText = `[Z] Syntax ${this.state.syntaxView.toUpperCase()}`;
        }

        let filterTitle = "No Filter";
        if (this.props.currentFilter !== null) {
            filterTitle = this.props.currentFilter.regex;
        }

        return (
            <div>
                <div className="toolbar row">
                    <div className="col-3">
                        <h3 className="float-left sec-title">{filterTitle}</h3>
                    </div>
                    <div className="col-9">
                        <button className="btn btn-outline-success float-right" onClick={this.onToggleHex}>{hexText}</button>
                        <button className="btn btn-outline-success float-right" onClick={this.onToggleSyntax}>{syntaxText}</button>
                        <button className="btn btn-outline-info float-right" onClick={this.onNextPage}>[U] Next Batch</button>
                        <button className="btn btn-outline-info float-right" onClick={this.onPreviousPage}>[Y] Previous Batch</button>
                    </div>
                </div>
                <div className="toolbar mt-1">
                    <button className="btn btn-outline-info" onClick={() => this.onServiceSelected(null)}>All Services</button>
                    {this.state.services.map(service => {
                        return <button
                            className="btn btn-outline-info"
                            onClick={() => this.onServiceSelected(service)}>
                            {service.name} ({service.port})
                        </button>
                    })}
                </div>
                <div className="row mt-3">
                    <div className="col-4 list-box">
                        <nav aria-label="breadcrumb">
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item active" aria-current="page">{this.state.offset}-{this.state.offset+limit} out of {this.state.connectionCount} connections</li>
                            </ol>
                        </nav>
                        <ResultList
                            connections={this.state.connections}
                            onItemSelect={this.onItemSelect}
                            selectedConnection={this.state.current}
                        />
                    </div>
                    <div className="col-8 content-box" id="content-box">
                        {this.state.current ? (
                            <ContentViewer
                                current={this.state.current}
                                hexView={this.state.hexView}
                                syntaxView={this.state.syntaxView}
                                filterPackets={this.state.filterPackets}
                            />
                        ) : (
                            <p>Select a connection</p>
                        )}
                    </div>
                </div>
            </div>

        );
    }
}