package main

import (
    "database/sql"
    "github.com/mattn/go-sqlite3"
    "gopkg.in/yaml.v2"
    "io/ioutil"
    "log"
    "os"
    "regexp"
)

type Config struct {
    DBPath string       `yaml:"db_path"`
    PCapPath string     `yaml:"pcap_path"`
    TempPath string     `yaml:"temp_path"`
    Filters []string    `yaml:"filters"`
    Services []Service  `yaml:"services"`
}

type Service struct {
    Name string     `yaml:"name" json:"name"`
    Port int        `yaml:"port" json:"port"`
}

func ReadConfig() Config {
    yamlFile, err := ioutil.ReadFile("config.yaml")
    if err != nil {
        log.Fatal("Cannot find configuration file! %v ", err)
    }
    var config Config
    err = yaml.Unmarshal(yamlFile, &config)
    if err != nil {
        log.Fatalf("Unmarshal: %v", err)
    }
    return config
}

func regexFunc(re, s string) (bool, error) {
    return regexp.MatchString(re, s)
}

func main() {
    log.Println("tarik is starting...")

    // Register regex extension
    sql.Register("sqlite3_custom",
        &sqlite3.SQLiteDriver{
            ConnectHook: func(conn *sqlite3.SQLiteConn) error {
                return conn.RegisterFunc("regexp", regexFunc, true)
            },
        })

    log.Println("Reading configuration...")

    config := ReadConfig()
    log.Printf("Target database: %s", config.DBPath)

    if _, err := os.Stat(config.DBPath); err == nil {
		log.Println("Using existing database")
	}else{
        log.Printf("Starting database generation from pcap: %s", config.PCapPath)
        if _, err := os.Stat(config.PCapPath); os.IsNotExist(err) {
            log.Fatal("Pcap file does not exist.")
        }

        log.Println("Invoking tcp-flow")
        InvokeTcpFlow(config.PCapPath, config.TempPath)

        log.Println("Starting the parsing process...")
        ParseAndPopulate(config)

        log.Println("Cleaning the temp folder...")
        RemoveContents(config.TempPath)

        log.Println("Initializing database...")
        InitializeDB(config)

        log.Println("Preprocessing connections...")
        ProcessConnections(config)
    }

    log.Println("Processing filters...")
    for _, filter := range config.Filters {
        ProcessFilter(config, filter)
    }

	StartServer(config)
}